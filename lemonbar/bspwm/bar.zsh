#!/bin/zsh

PLATFORM_BAR_DIR=$(dirname $(realpath $0))/..
ARCH_BAR_DIR=$PLATFORM_BAR_DIR/../arch/lemonbar
BAR_DIR=$PLATFORM_BAR_DIR/../base/lemonbar

BAR_HEIGHT=18
BAR_FONT_SIZE=8

source $BAR_DIR/bspwm/functions.zsh

init

(while :; do echo "pacman"; sleep 5m; done) >&$SELECT_FD &

(while read -r line <&$SELECT_FD; do
  if [ "$line" = "tick" ]; then
    on_tick
    DROPBOX=$(dropbox_status)
  elif [ "$line" = "pacman" ]; then
    PACMAN=$($ARCH_BAR_DIR/pacman.sh)
  else
    on_bspwm_report
  fi
  pre_render
  render_left ""
  render_right " %{F$BLUE}\ue0aa%{F-} $DROPBOX %{A:$ARCH_BAR_DIR/dzen/pacman.sh:}%{F$BLUE}\ue00f%{F-} $PACMAN%{A}" ""
done) | invoke_lemonbar
