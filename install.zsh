#!/bin/zsh

single_key_prompt() {
  old_stty=$(stty -g)
  stty raw -echo; answer=$(head -c 1); stty $old_stty
  echo $answer
}

SELF_DIR=$(dirname $(realpath $0))

pushd $SELF_DIR

source base/install_functions.zsh

pamd_login=$(cat /etc/pam.d/login)
if ! (echo "$pamd_login" | sed -n '/^auth\s\+optional\s+pam_gnupg.so$/q0;q1') && \
     (echo "$pamd_login" | sed -n '/^session\s\+optional\s+pam_gnupg.so$/q0;q1') && \
     pacman -Q gnome-keyring &>/dev/null && pacman -Q pam-gnupg-git &>/dev/null; then
  echo -n "add gnupg settings to /etc/pam.d/login? [Y/n] "
  answer=$(single_key_prompt)
  echo
  if [ "$answer" != "n" -a "$answer" != "N" ]; then
    EDITOR="vi -c 'normal Goauth       optional     pam_gnupg.sosession    optional     pam_gnupg.so'" sudoedit /etc/pam.d/login
    yay gnome-keyring
    yay pam-gnupg-git
    echo "be sure to check that ~/.pam-gnupg keygrips are what you want with `gpg -K --with-keygrip`"
  fi
fi

if ! (cat ~/.gnupg/gpg-agent.conf | grep -q allow-preset-passphrase); then
  echo -n "add pam setting to gpg-agent.conf? [Y/n] "
  answer=$(single_key_prompt)
  echo
  if [ "$answer" != "n" -a "$answer" != "N" ]; then
    vi -c 'normal Goallow-preset-passphrase' ~/.gnupg/gpg-agent.conf
    echo "be sure to check that ~/.pam-gnupg keygrips are what you want with `gpg -K --with-keygrip`"
  fi
fi

typeset -A platform_symlink_files
typeset -A platform_symlink_prefixes
platform_symlink_files=(
  ".config/bspwm/bspwmrc" "bspwmrc"
)
platform_symlink_prefixes=(
  ".config/bspwm/bspwmrc" "."
)
for file in $(ls -d .* | grep -v ".git"); do
  platform_symlink_files[$file]=$file
  platform_symlink_prefixes[$file]="."
done

PORTED_FILE_DICT=true
source arch/install.zsh
source base/install.sh

popd
